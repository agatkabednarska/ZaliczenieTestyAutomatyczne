package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageWithMenu {
    protected final WebDriver driver;

    protected PageWithMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".menu-workspace")
    private WebElement workspaceNav;

    @FindBy(css="a[href$=Projects]")
    private WebElement processesMenu;

    @FindBy(css="a[href$=Characteristics]")
    private WebElement characteristicsMenu;

    @FindBy(css=".menu-home")
    private WebElement homeNav;

    @FindBy(css=".menu-home+ul>li:first-child>a")
    private WebElement dashboardMenu;

    private boolean isParentExpanded(WebElement menuLink) {
        WebElement parent = menuLink.findElement(By.xpath("./.."));

        return parent.getAttribute("class").contains("active");
    }

    public ProcessesPage goToProcesses(){
        if(!isParentExpanded(workspaceNav))
            workspaceNav.click();

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(processesMenu));

        processesMenu.click();

        return new ProcessesPage(driver);
    }

    public CharacteristicsPage goToCharacteristics() {
        if (!isParentExpanded(workspaceNav)) {
            workspaceNav.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicsMenu));

        characteristicsMenu.click();

        return new CharacteristicsPage(driver);
    }

    public DashboardsPage goToDashboard(){
        if(!isParentExpanded(homeNav)) {
            homeNav.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardMenu));

        dashboardMenu.click();

        return new DashboardsPage(driver);

    }}
