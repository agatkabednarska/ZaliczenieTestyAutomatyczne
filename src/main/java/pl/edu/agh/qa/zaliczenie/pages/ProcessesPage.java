package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.xml.sax.helpers.NamespaceSupport;

import java.util.concurrent.TimeUnit;

public class ProcessesPage extends PageWithMenu {
    public ProcessesPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a[href$=\\/Projects\\/Create]")
    private WebElement addNewProcessBtn;

    @FindBy(css = ".table tbody tr:last-child td:first-child")
    private WebElement nameElm;

    @FindBy(css = ".table tbody tr:last-child a[href*=Delete]")
    private WebElement deleteLastElm;

    @FindBy(css = "#crud-modal button[value=Delete]")
    private WebElement deleteApprovalBtn;

    public CreateProcessPage addNewProcess() {
        addNewProcessBtn.click();

        return new CreateProcessPage(driver);
    }

    public ProcessesPage assertGoToProcessesUrl () {
        Assert.assertTrue(driver.getCurrentUrl().contains("/Projects"));

        return this;
    }

    public ProcessesPage assertProcessCreated(String name) {
        Assert.assertEquals(nameElm.getText(), name);

        return this;
    }

    public ProcessesPage deleteCreated() {
        WebDriverWait wait = new WebDriverWait(driver, 5);

        wait.until(ExpectedConditions.elementToBeClickable(deleteLastElm));
        deleteLastElm.sendKeys(Keys.ENTER);

        wait.until(ExpectedConditions.elementToBeClickable(deleteApprovalBtn));
        deleteApprovalBtn.click();

        return this;
    }
}
