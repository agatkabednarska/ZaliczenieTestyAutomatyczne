package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.ObjectInputStream;

public class CreateProcessPage {
    private WebDriver driver;

    CreateProcessPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#Name")
    private WebElement processNameTxt;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    public CreateProcessPage processName(String name) {
        processNameTxt.clear();
        processNameTxt.sendKeys(name);

        return this;
    }

    public ProcessesPage createProcess() {
        createBtn.click();

        return new ProcessesPage(driver);
    }
}
