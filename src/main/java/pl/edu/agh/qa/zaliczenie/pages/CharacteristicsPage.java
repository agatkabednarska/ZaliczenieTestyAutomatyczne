package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CharacteristicsPage extends PageWithMenu {
    public CharacteristicsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public CharacteristicsPage assertGoToCharacteristicsUrl () {
        Assert.assertTrue(driver.getCurrentUrl().contains("/Characteristics"));

        return this;
    }
}
