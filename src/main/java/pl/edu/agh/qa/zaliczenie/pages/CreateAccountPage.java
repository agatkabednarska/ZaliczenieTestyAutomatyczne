package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {
    private WebDriver driver;

    CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy (id= "ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(css = "#register .validation-summary-errors li")
    private List<WebElement> errorElm;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public CreateAccountPage typeConfirmPassword(String password) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);

        return this;
    }

    public HomePage submitRegister() {
        registerBtn.click();

        return new HomePage(driver);
    }

    public CreateAccountPage focusPassword() {
        passwordTxt.click();
        return this;
    }


    public CreateAccountPage assertErrorTxtIsShown(String errorTxt) {
        Assert.assertEquals(errorElm.size(), 1);
        Assert.assertEquals(errorElm.get(0).getText(), errorTxt);

        return this;
    }

    public CreateAccountPage submitRegisterWithFailure() {
        registerBtn.click();

        return this;

    }
}

