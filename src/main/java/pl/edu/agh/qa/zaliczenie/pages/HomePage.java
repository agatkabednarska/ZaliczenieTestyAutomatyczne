package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class HomePage extends PageWithMenu {
    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".profile_info>h2")
    private WebElement welcomeElm;

    public HomePage assertWelcomElementIsShown () {
        Assert.assertTrue(welcomeElm.getText().contains("Welcome"));

        return this;
    }

}

