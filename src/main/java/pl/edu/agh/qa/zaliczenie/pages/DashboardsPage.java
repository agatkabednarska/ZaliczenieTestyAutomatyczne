package pl.edu.agh.qa.zaliczenie.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class DashboardsPage extends PageWithMenu {
    public DashboardsPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".x_title > h2")
    private WebElement demoProjectElm;


    public DashboardsPage assertGoToDashboardUrl() {
        Assert.assertTrue(demoProjectElm.getText().contains("DEMO PROJECT"));

        return this;
    }
}
