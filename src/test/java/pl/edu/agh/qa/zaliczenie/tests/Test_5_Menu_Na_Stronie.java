package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

public class Test_5_Menu_Na_Stronie extends SeleniumBaseTest {

    @Test
    public void menuTest () {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .assertGoToProcessesUrl()
                .goToCharacteristics()
                .assertGoToCharacteristicsUrl()
                .goToDashboard()
                .assertGoToDashboardUrl();
    }
}
