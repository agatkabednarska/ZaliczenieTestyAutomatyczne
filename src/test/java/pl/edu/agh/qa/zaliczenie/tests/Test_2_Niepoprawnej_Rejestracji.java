package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.CreateAccountPage;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

import java.util.UUID;

public class Test_2_Niepoprawnej_Rejestracji extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] loginDetails() {
        return new Object[][] {
                {"test__2.1__testAgata.com", "Test1!", "Test1!", "The Email field is not a valid e-mail address."},
                {"test__2.2__@", "Test2!", "Test2!", "The Email field is not a valid e-mail address."},
                {"test__2.3__@testAgata.com", "Test2!", "Test1!", "The password and confirmation password do not match."},
                {"test__2.4__@testAgata.com", "T!", "T!", "The Password must be at least 6 and at max 100 characters long."},
                {"test__2.5__@testAgata.com", "", "", "The Password field is required."},
                {"", "Test1!", "Test1!", "The Email field is required."}
        };
    }

    @Test(dataProvider = "loginDetails")
    public void IncorrectRegistration(String login, String password, String confirmPassword, String errorTxt) {

        new LoginPage(driver)
                .goToRegister()
                .typeEmail(login)
                .typePassword(password)
                .typeConfirmPassword(confirmPassword)
                .focusPassword()
                .submitRegisterWithFailure()
                .assertErrorTxtIsShown(errorTxt);
    }
}
