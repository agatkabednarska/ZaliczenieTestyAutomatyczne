package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

public class Test_6_Poprawnego_Dodania_Procesu extends SeleniumBaseTest {

    @Test
    public void menuTest() {
        String processName = "AgataTest";

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .goToProcesses()
                .addNewProcess()
                .processName(processName)
                .createProcess()
                .assertProcessCreated(processName)
                .deleteCreated();
    }

}
