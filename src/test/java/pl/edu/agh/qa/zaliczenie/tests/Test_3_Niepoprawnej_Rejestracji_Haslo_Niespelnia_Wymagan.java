package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.CreateAccountPage;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

import java.util.UUID;

public class Test_3_Niepoprawnej_Rejestracji_Haslo_Niespelnia_Wymagan extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] loginDetails() {
        return new Object[][]{
                {"test__3.1__@testAgata.com", "test1!", "test1!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"test__3.2__@testAgata.com", "Test!!", "Test!!", "Passwords must have at least one digit ('0'-'9')."},
                {"test__3.3__@testAgata.com", "Test12", "Test12", "Passwords must have at least one non alphanumeric character."},
        };
    }

    @Test(dataProvider = "loginDetails")
    public void IncorrectRegistration(String login, String password, String confirmPassword, String errorTxt) {

        new LoginPage(driver)
                .goToRegister()
                .typeEmail(login)
                .typePassword(password)
                .typeConfirmPassword(confirmPassword)
                .focusPassword()
                .submitRegisterWithFailure()
                .assertErrorTxtIsShown(errorTxt);
    }


}