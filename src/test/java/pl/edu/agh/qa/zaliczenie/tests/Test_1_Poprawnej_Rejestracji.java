package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.CreateAccountPage;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

import java.util.UUID;

public class Test_1_Poprawnej_Rejestracji extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] loginDetails() {
        return new Object[][] {
                {"test" + randomString() +"@testAgata.com", "Test1!"},
                {"test" + randomString() +"@testAgata.com", "Test2!"}
        };
    }

    @Test(dataProvider = "loginDetails")
    public void CorrectRegistration(String login, String password) {

        new LoginPage(driver)
                .goToRegister()
                .typeEmail(login)
                .typePassword(password)
                .typeConfirmPassword(password)
                .submitRegister()
                .assertWelcomElementIsShown();

    }
}

