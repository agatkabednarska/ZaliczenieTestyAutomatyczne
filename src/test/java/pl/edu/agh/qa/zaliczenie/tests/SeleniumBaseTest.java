package pl.edu.agh.qa.zaliczenie.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest{
    WebDriver driver;

    @BeforeMethod
    public void baseBeforeMethod() {
        //System.setProperty("webdriver.chrome.driver", "C:\\dev\\driver\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Agatka\\Downloads\\chromedriver_win32\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("http://localhost:4444/");
    }

    static String randomString() {
        return UUID.randomUUID().toString().substring(0, 8);
    }

    @AfterMethod
    public void baseAfterMethod() {
        driver.quit();
    }
}
