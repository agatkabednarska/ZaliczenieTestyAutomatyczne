package pl.edu.agh.qa.zaliczenie.tests;

import org.testng.annotations.Test;
import pl.edu.agh.qa.zaliczenie.pages.LoginPage;

public class Test_4_Poprawnego_Logowania extends SeleniumBaseTest {

    @Test
    public void correctLoginTestWithChaining() {

        new LoginPage(driver)
                .typeEmail("test@test.com")
                .typePassword("Test1!")
                .submitLogin()
                .assertWelcomElementIsShown();
    }
}